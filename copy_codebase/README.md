# Copy Codebase Utility

A simple utility to copy codebase. Used mostly to facilitate the following -
- ROV/CM software audit
- Remote debugging

## Usage

1. Copy this script on your Linux machine to any path (e.g. ~/Desktop)

2. If not an executable already, make it executable by running command -

	$ chmod a+x codebase_copy.sh

3. Run the script by running -

	$ ./codebase_copy.sh

This may take a few minutes. Please be patient.

4. Voila! Your archived file should be ready once the previous command returns and you're able to enter new commands on your command prompt.

## Future Scope

1. User should be able to enter path (default ~/Documents)

2. User should be able to enter filename (default Codebase)

3. User should be able to enter archiving format (default tar.gz)
