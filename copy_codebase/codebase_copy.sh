#!/bin/bash

## Find tar utility location
archive_util=$(which tar)
## tar command params
cmd_params='-czvf'

## Go to Documents
cd ~/Documents/

## Getting archived filename
orig_filename='Codebase'
echo "Original filename: ${orig_filename}"
filename=${orig_filename}
filename+='_'
filename+=$(date +%d-%m-%Y_%H-%M)
filename+='.tar.gz'
orig_filename+='/'

## Done, print some variables before we get to real work
echo "New file to be created: ${filename}"
## Get inside Documents
echo "Inside $(pwd)"
## Run the command to archive
${archive_util} ${cmd_params} ${filename} ${orig_filename}
## Print a friendly message
echo "We're done!"
