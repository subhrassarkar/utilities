# Crack Length Measurement - Zipped File Upload - Script

## Brief

The following script(s) emulate zipped file upload and subsequent download from
SRG hosted servers for the Crack Length Measurement project.

## How to use it?

## Future scope

1. For now, we've added support only for "Zip archive data". Going forward, we
may support other formats (e.g. gzip compressed data, bzip2 compressed data
etc.)
