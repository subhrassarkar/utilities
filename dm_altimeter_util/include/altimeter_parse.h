/*
 * @brief	Header file for model specific parsing logic for Altimeter sensors
 *
 * @name	Subhra Sankha Sarkar <subhra.s@planystech.com>
 *
 * @date	14-09-2021
 *
 * @version	1.0
 */

#ifndef ALTIMETER_PARSE_H
#define ALTIMETER_PARSE_H

using namespace std;

class altimeter_parser {
	private:
		double altimeter_reading;
	public:
		/* To set the altimeter reading */
		bool setAltimeterReading(double );

		/* To retrieve altimeter readings for Impact Subsea Sonars - $ISADS strings */
		/* Sample format => $ISADS,ddd.ddd,M,tt.t,C*xx<CR><LF>	*/

		double getAltimeterValue_ISADS(string str);

		/* To retrieve altimeter readings for Impact Subsea Sonars - $ISADI strings */
		/* Sample format => $ISADI,ddd.ddd,M,e.eeee,c.cccc,tt.t,C*xx<CR><LF>	*/

		double getAltimeterValue_ISADI(string str);

		/* To retrieve altimeter readings for Impact Subsea Sonars - $ISADI strings */
		/* Sample format => $ISAMD,tt.t,C,ddd.ddd,...*xx<CR><LF>	*/

		double getAltimeterValue_ISAMD(string str);

		/* To retrieve altimeter readings for Tritech Sonars - $PADBT strings */
		/* Sample format => $PADBT,000.000,f,000.00,M,000.000,F*30<CR><LF>	*/

		double getAltimeterValue_PADBT(string str);
};

#endif /* ALTIMETER_PARSE_H */

