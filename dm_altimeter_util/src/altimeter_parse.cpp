/*
 * @brief	Model specific parsing logic for Altimeter sensors
 *
 * @name	Subhra Sankha Sarkar <subhra.s@planystech.com>
 *
 * @date	14-09-2021
 *
 * @version	1.0
 */

#include <iostream>
#include <string>
#include <algorithm>

#include "altimeter_parse.h"

using namespace std;

/*
 * @brief	Set Altimeter reading value (vendor agnostics)
 *
 * @param	[IN] double - Value to set
 *
 * @return	[OUT] boolean - True by default
 */
bool altimeter_parser::setAltimeterReading(double reading)
{
	this->altimeter_reading = reading;
	return true;
}

/*
 * @brief	Get altimeter reading value (Impact Subsea).
 * 		Sting format: $ISADS,ddd.ddd,M,tt.t,C*xx<CR><LF>
 *
 * @param	[IN] string - Line buffer output captured from the device
 *
 * @return	[OUT] double - Current altimeter reading
 */
double altimeter_parser::getAltimeterValue_ISADS(string str)
{
	double ret = 0.0;
	/* Base case */
	if (str.length() == 0) {
		cout << "Null string returned\n";
		return ret;
	}
	/* Otherwise */
	cout << "Parsing: " << str << "\n";
	try {
		ret = stod(str.substr(7, 7));
	} catch (const std::invalid_argument& ia) {
		cout << "Invalid argument: " << ia.what() << '\n';
		ret = 0.0;
	}
	return ret;
}

/*
 * @brief	Get altimeter reading value (Impact Subsea).
 * 		Sting format: $ISADI,ddd.ddd,M,e.eeee,c.cccc,tt.t,C*xx<CR><LF>
 *
 * @param	[IN] string - Line buffer output captured from the device
 *
 * @return	[OUT] double - Current altimeter reading
 */
double altimeter_parser::getAltimeterValue_ISADI(string str)
{
	double ret = 0.0;
	/* Base case */
	if (str.length() == 0) {
		cout << "Null string returned\n";
		return ret;
	}
	/* Otherwise */
	cout << "Parsing: " << str << "\n";
	try {
		ret = stod(str.substr(7, 7));
	} catch (const std::invalid_argument& ia) {
		cout << "Invalid argument: " << ia.what() << '\n';
		ret = 0.0;
	}
	return ret;
}

/*
 * @brief	Get altimeter reading value (Impact Subsea).
 * 		Sting format: $ISAMD,tt.t,C,ddd.ddd,...*xx<CR><LF>
 *
 * @param	[IN] string - Line buffer output captured from the device
 *
 * @return	[OUT] double - Current altimeter reading
 */
double altimeter_parser::getAltimeterValue_ISAMD(string str)
{
	double ret = 0.0;
	/* Base case */
	if (str.length() == 0) {
		cout << "Null string returned\n";
		return ret;
	}
	/* Otherwise */
	cout << "Parsing: " << str << "\n";
	try {
		ret = stod(str.substr(14, 7));
	} catch (const std::invalid_argument& ia) {
		cout << "Invalid argument: " << ia.what() << '\n';
		ret = 0.0;
	}
	return ret;
}

/*
 * @brief	Get altimeter reading value (Impact Subsea).
 * 		Sting format: $PADBT,000.000,f,000.00,M,000.000,F*30<CR><LF>
 *
 * @param	[IN] string - Line buffer output captured from the device
 *
 * @return	[OUT] double - Current altimeter reading
 */
double altimeter_parser::getAltimeterValue_PADBT(string str)
{
	double ret = 0.0;
	/* Base case */
	if (str.length() == 0) {
		cout << "Null string returned\n";
		return ret;
	}
	/* Otherwise */
	cout << "Parsing: " << str << "\n";
	try {
		ret = stod(str.substr(14, 6));
	} catch (const std::invalid_argument& ia) {
		cout << "Invalid argument: " << ia.what() << '\n';
		ret = 0.0;
	}
	return ret;
}

