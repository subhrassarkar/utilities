/*
 * @brief	To test altimeter parsing
 *
 * @param	None
 *
 * @return	0
 */

#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>

#include "altimeter_parse.h"

using namespace std;

int main(int argc, char *argv[])
{
	altimeter_parser 	ap;
	double			altimeter_value;

	std::string line;
	std::ifstream infile("../testdata/altimeter_capture.txt");


	while (getline(infile, line)) {
		cout << line;
		if (line.substr(1,5).compare("ISADS") == 0) {
			altimeter_value = ap.getAltimeterValue_ISADS(line);
			ap.setAltimeterReading(altimeter_value);
			cout << "\t Altimeter rading: " << altimeter_value;
		} else if (line.substr(1,5).compare("ISADI") == 0) {
			altimeter_value = ap.getAltimeterValue_ISADI(line);
			ap.setAltimeterReading(altimeter_value);
			cout << "\t Altimeter rading: " << altimeter_value;

		} else if (line.substr(1,5).compare("ISAMD") == 0) {
			altimeter_value = ap.getAltimeterValue_ISAMD(line);
			ap.setAltimeterReading(altimeter_value);
			cout << "\t Altimeter rading: " << altimeter_value;

		} else if (line.substr(1,5).compare("PADBT") == 0) {
			altimeter_value = ap.getAltimeterValue_PADBT(line);
			ap.setAltimeterReading(altimeter_value);
			cout << "\t Altimeter rading: " << altimeter_value;

		} else {
			// Nothing to do here
		}	
		cout << "\n";
	}

	return 0;
}
