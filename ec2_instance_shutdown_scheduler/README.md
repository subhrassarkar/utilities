# EC2 Instance Shutdown Scheduler


## What is it?

A simple utility to proactively turn-off underutilized EC2 instances.

## How does it work?

The algorithm takes into account of the following -

1. Resource utilization for the past 1 minute
2. Resource utilization for the past 5 minutes
3. Resource utilization for the past 15 minutes

Based on this heuristic, it checks if the CPU utilization has spiked during the 
last 10 minutes, meaning more activity during this period. So, if there is a
spike in activity, we shouldn't turn it off. However, if there is hardly any
activity for a duration of 15 minutes, no point keeping the EC2 instance alive.

## What are the threasholds for shutting down the instance?

The thresholds are determined by the workload and will vary based on the
application being run.

## How to run it?

This utility should run as a background process through SystemD. User needn't
interact with it manually.

## What are it's limitations?

Below is the list of known limitations -

1. Works best for single core system
2. Doesn't do any exception handling
3. SystemD glue is missing
4. (in)activity threshold fine-tuning
