#!/usr/bin/env python3

import subprocess as sp

# By default, let's keep the flag disabled
shutdown_flag = False

# Get the CPU load for last 1, 5 & 15 mins
load_str = sp.getoutput('/usr/bin/w | /usr/bin/head -n1 | /usr/bin/awk -F\' \' \'{ print $8 $9 $10 }\'')
print(load_str)
# https://stackoverflow.com/a/48798650 - Grab output and convert it into a list of floats
cpu_load = list(map(float, load_str.split(',')))

# Decision making - Need feedback from Akshaya
if cpu_load[0] < 0.2 and ((cpu_load[0] - cpu_load[1]) > 0.1) and (abs(cpu_load[2] - cpu_load[1]) < 0.05):
    shutdown_flag = True

if shutdown_flag:
    print(f"Time to shut it down")
else:
    print(f"Let's keep it running")
