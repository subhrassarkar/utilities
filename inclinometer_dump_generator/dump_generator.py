#!/usr/bin/python3

import random as rd
import time as time

## Function to generate random numbers in the range -90 to 90 with 2 digit precision after decimal point

def return_rand_num():
    return round(rd.uniform(-90, 90), 2)

## Function to populate the dump file

def write_dump_file():
    filename = "inclinometer_data.dmp"
    count = 0

    ## Open file handle
    with open(filename, 'w') as f:
        while count < 50000:
            ## Write 50000 lines to it
            message = "$PTINC," + str(return_rand_num()) + "*ff"
            f.write(message + '\n')
            time.sleep(1/50000)
            count += 1


## Driver function

def main():
    print("Starting to write the dump file...");
    write_dump_file()
    print("Writing to dump file completed successfully")


if __name__ == "__main__":
    main()

