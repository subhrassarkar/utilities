# Storage Utilization

## **Brief**

A simple utility to check the current storage utilization of the system.

## **How to use it?**

Simply run it as a background process -

```$ ./util_storage_util.sh &```

Upon running the above script, it'll generate file -

```/tmp/CM_storage_utilization.txt```

**OR**

```/tmp/CM_storage_utilization.txt```

And a typical file looks as below -

![Storage Utilization](ROVCM_Storage_Utilization.png)

## **Future Scope**

1. Make it dynamic by determining whether it's CM or ROV from hostname
2. Integrate it with systemd
