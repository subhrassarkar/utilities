################################################################################
##									      ##
## @brief	A script to check current storage utilization on the system   ##
##									      ##
## @author	Subhra Sankha Sarkar <subhra.s@planystech.com>		      ##
##									      ##
## @date	03-09-2021                                                    ##
##                                                                            ##
## @revision	0.2                                                           ##
##									      ##
################################################################################

#!/bin/bash

## Specify whether it's ROV or CM
rov_or_cm="CM"

## Check current storage utilization
while :
do
	df -h --exclude={tmpfs,devtmpfs,squashfs} --total | tail -n 1 | awk -F" " '{print "Used: "$3"\nTotal: "$2"\nPercentage: "$5}'> /tmp/${rov_or_cm}_storage_utilization.txt
	## Run every 10 seconds
	sleep 10
done
