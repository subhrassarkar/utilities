## Utility to convert vidoes in specified formats

**How to use?**

Run the script as below -

    ./video_converter <param1> <param2>

where -

    param1 - Absolute path where the videos are stored

    param2 - Output video format (e.g. mp4, mov etc.)
