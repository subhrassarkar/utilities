#!/bin/bash

## Help
help() {
	printf "[Usage] \n\n"
	printf "\t./video_converter.sh <param1> <param2>\n\n"
	printf "\twhere -\n\n"
	printf "\t\tparam1 - Absolute path of the video directory\n"
	printf "\t\tparam2 - Output video format (e.g. mp4, mov, webm etc)\n"
}

## Convert to output format
convert_to_regular_video() {
	/usr/bin/ffmpeg -i $1 -c copy $2.$3
}

## Convert to WebM output format
convert_to_webm_video() {
	/usr/bin/ffmpeg -i $1 -b:v 1M -q:v 10 -vcodec libvpx -acodec libvorbis ./webm/$2.$3
}

## Check number of parameters
if (( $# != 2)); then
	2>&1 printf "[Error] Incorrect number of parameters\n\n"
	help
	exit -1
fi

printf "Destination path: %s\n" $1
printf "Extension: %s\n" $2

destination_path=$1
extension=$2

## Enter destination path
cd ${destination_path}
printf "Current directory: %s\n" $(pwd)
current_file="dummy_filename.dummy_extension"
## Iterate through all the files in the directory
for file in *; do
	if [[ -f "$file" ]]; then
		current_file=$file
		raw_filename=`printf ${current_file} | awk -F"." '{print $1}'`
		printf "Current filename: %s\n" ${raw_filename}
		## Time to call conversion function
		if [[ $2 -eq "webm" ]]; then
			convert_to_webm_video ${current_file} ${raw_filename} ${extension}
		else
			convert_to_regular_video ${current_file} ${raw_filename} ${extension}
		fi
		printf "\tInput file: %s, Converted Output file: %s.%s\n" ${current_file} ${raw_filename} ${extension}
	fi
done
